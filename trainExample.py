import random
import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen


# html = urlopen("https://morvanzhou.github.io/static/scraping/basic-structure.html").read().decode('utf-8')
# print(html)

# 得到所有地方航班及链接
def getAllFlights():
    flights = {}  # {'安庆航班': 'https://flights.ctrip.com/schedule/aqg..html', ...}
    url = 'https://flights.ctrip.com/schedule'
    UserAgents = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36'
    ]
    headers = {
        'user-agent': random.choice(UserAgents),
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.9',
        'upgrade-insecure-requests': '1'
    }
    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.text, 'lxml')
    letter_list = soup.find(attrs={'class': 'letter_list'}).find_all('li')
    for li in letter_list:
        for a in li.find_all('a'):
            flights[a.get_text()] = url + a['href'][9:]
    return flights


# 得到一个地方航班的所有线路
def getFlightLines(url):
    UserAgents = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36'
    ]
    flightlines = {}  # {'安庆-北京': 'http://flights.ctrip.com/schedule/aqg.bjs.html', ...｝
    headers = {
        'Referer': 'https://flights.ctrip.com/schedule/',
        'user-agent': random.choice(UserAgents)
    }
    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.text, 'lxml')
    letter_list = soup.find(attrs={'id': 'ulD_Domestic'}).find_all('li')
    for li in letter_list:
        for a in li.find_all('a'):
            flightlines[a.get_text()] = a['href']

    return flightlines


# print(getFlightLines('https://flights.ctrip.com/schedule/aqg..html'))

# 得到这条线路的所有航班信息
def getFlightInfo(url):
    UserAgents = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36'
    ]
    flightInfos = []
    headers = {
        'Host': 'flights.ctrip.com',
        'user-agent': random.choice(UserAgents)
    }
    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.text, 'lxml')
    flights_tr = soup.find(attrs={'id': 'flt1'}).find_all('tr')
    for tr in flights_tr:  # 遍历每个一航班
        flightInfo = {}
        info_td = tr.find_all('td')
        # 航班编号
        flight_no = info_td[0].find('strong').get_text().strip()
        flightInfo['flight_no'] = flight_no
        # 起飞时间
        flight_stime = info_td[1].find('strong').get_text().strip()
        flightInfo['flight_stime'] = flight_stime
        # 起飞机场
        flight_sairport = info_td[1].find('div').get_text().strip()
        flightInfo['flight_sairport'] = flight_sairport
        # 降落时间
        flight_etime = info_td[3].find('strong').get_text().strip()
        flightInfo['flight_etime'] = flight_etime
        # 降落机场
        flight_eairport = info_td[3].find('div').get_text().strip()
        flightInfo['flight_eairport'] = flight_eairport
        # 班期
        flight_schedule = []
        for s in info_td[4].find(attrs={'class': 'week'}).find_all(name='span', attrs={'class': 'blue'}):
            flight_schedule.append(s.get_text().strip())
        flight_schedule = ' '.join(flight_schedule)
        flightInfo['flight_schedule'] = flight_schedule
        # 准点率
        flight_punrate = info_td[5].get_text().strip()
        flightInfo['flight_punrate'] = flight_punrate
        # 价格
        flight_price = info_td[6].get_text().strip()
        flightInfo['flight_price'] = flight_price

        flightInfos.append(flightInfo)
    return flightInfos


print(getFlightInfo('https://flights.ctrip.com/domestic/schedule/aqg.kwl.p1.html'))
