from geopy.distance import geodesic
from geopy.distance import great_circle
from math import *

# Cardiff	Brinton	5162.601017
# Cardiff	51.48158100000001	-3.17909
# Brinton	33.3965597	-111.5828991

# Loading the lat-long data for Kolkata & Delhi
kolkata = (51.481581000000, -3.17909)
delhi = (33.3965597, -111.5828991)

# Print the distance calculated in km
print(geodesic(kolkata, delhi).miles)
print(great_circle(kolkata, delhi).miles)


def HaversineDistance(location1, location2):
    """Method to calculate Distance between two sets of Lat/Lon."""
    lat1, lon1 = location1
    lat2, lon2 = location2
    earth = 6371  # Earth's Radius in Kms.
    # Calculate Distance based in Haversine Formula
    # dlat = math.radians(lat2 - lat1)
    # dlon = math.radians(lon2 - lon1)
    # a = math.sin(dlat / 2) * math.sin(dlat / 2) + math.cos(math.radians(lat1)) * math.cos(
    #     math.radians(lat2)) * math.sin(dlon / 2) * math.sin(dlon / 2)
    # c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    # d = earth * c
    # return d
