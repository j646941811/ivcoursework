import pymysql
import requests
from bs4 import BeautifulSoup
import csv
import json
import re
import multiprocessing as mp
import time

allNeedHeader = ['departureStationName', 'departureStationCRS', 'arrivalStationName', 'arrivalStationCRS',
                 'departureTime', 'arrivalTime', 'durationHours', 'durationMinutes', 'changes',
                 'breakdownType', 'fareTicketType', 'fareProvider', 'ticketPrice']


def getInitUrl():
    # link fomat : 'http://ojp.nationalrail.co.uk/service/timesandfares/leeds/London/140120/0000/dep'
    url1 = 'http://ojp.nationalrail.co.uk/service/timesandfares/'
    # date
    url2 = '/140120/'
    url3 = '/dep'
    UserAgents = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36'
    ]
    headers = {
        'user-agent': UserAgents[0],
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.9,zh-TW;q=0.8,en-US;q=0.7,en;q=0.6',
        'upgrade-insecure-requests': '1',
        'host': 'ojp.nationalrail.co.uk'
    }
    return {
        'url1': url1,
        'url2': url2,
        'url3': url3,
        'headers': headers
    }


def getJourneys(city_url, departure_time='0000', original_time=0000, journey_infos=None):
    if journey_infos is None:
        journey_infos = []
    init_url_dict = getInitUrl()
    url = init_url_dict['url1'] + city_url + init_url_dict['url2'] + departure_time + init_url_dict['url3']
    print('start to request ' + url)
    # only return 5 Journeys after the specific time(0000) each request (So the bug needs to be fixed here)
    response = requests.get(url, headers=init_url_dict['headers'])
    soup = BeautifulSoup(response.text, features='lxml')
    data_list = soup.find(attrs={'id': 'oft'}).find('tbody').find_all('tr', {'class': re.compile('.*mtx.*')})
    train_info = {}
    for key, rowData in enumerate(data_list):
        td_list = rowData.find_all('td')
        # don not delete this comment!!!
        # train_info['dep'] = td_list[0].get_text().strip()  # dep time
        # train_info['from'] = td_list[1].get_text().strip() + td_list[1].find('abbr').get_text().strip()  # from city
        # train_info['to'] = td_list[2].get_text().strip() + td_list[2].find('abbr').get_text().strip()  # to city
        # train_info['arr'] = td_list[3].get_text().strip()  # arr time
        # train_info['dur'] = td_list[4].get_text().strip()  # dur time
        # train_info['info'] = td_list[6].find('a').get_text().strip()  # details info
        train_info['allinfo'] = td_list[8].find('script').get_text().strip()  # all info
        all_info_dict = json.loads(train_info['allinfo'])
        all_info = {}
        all_need_info = {}
        # put journey info and fare info together
        all_info.update(all_info_dict['jsonJourneyBreakdown'])
        if len(all_info_dict['singleJsonFareBreakdowns']) != 0:
            all_info.update(all_info_dict['singleJsonFareBreakdowns'][0])
        # only reserve what we need
        for item in allNeedHeader:
            if item in all_info.keys():
                all_need_info[item] = all_info[item]
            else:
                all_need_info[item] = 'NaN'
        # decide if it should be appended，exclude the data in the next day
        if departure_time == '0000' and key == 0:
            original_time = int(all_need_info['departureTime'].replace(':', ''))
        departure_time = int(all_need_info['departureTime'].replace(':', ''))
        if departure_time >= original_time:
            original_time = departure_time
            journey_infos.append(all_need_info)
        else:
            print(city_url + ' is done')
            return journey_infos
    # This is the program to fix the bug
    # get the time of the last journey and add 6 minutes as the condition for the next request,
    # if not, it would result some duplications in the journeys
    # 6 minutes is really tricky!!! But this is the truth in the website
    print('get ' + str(departure_time) + ' data successful')
    new_departure_time = int(journey_infos[-1]['departureTime'].replace(':', '')) + 6
    if new_departure_time < 2359:
        new_departure_time = str(new_departure_time).zfill(4)
        if new_departure_time[2] == '6':
            # get hour from departureTime
            hour = int(new_departure_time[0] + new_departure_time[1])
            hour += 1
            new_departure_time = (str(hour) + '0' + new_departure_time[3]).zfill(4)
        print('next departureTime ' + new_departure_time)
        # Notice : there is a pitfall in the recursion (about the use of return)！
        return getJourneys(city_url, new_departure_time, original_time, journey_infos)
    else:
        return journey_infos


def writeCsv(result, filename):
    print('start write ' + filename)
    with open(filename, "w", encoding='utf8', newline='') as outFileCsv:
        # csv header
        file_header = allNeedHeader
        out_dict_writer = csv.DictWriter(outFileCsv, file_header)
        out_dict_writer.writeheader()
        # csv body, the format of the data here is dict
        out_dict_writer.writerows(result)
        outFileCsv.close()
        print('write success!!!')


def sql(dictList):
    sqlStr = ''
    values = ''
    for dict in dictList:
        for item in allNeedHeader:
            sqlStr = sqlStr + "'" + str(dict[item]) + "',"
        values = values + '(' + sqlStr[:-1] + '),'
    values = values[:-1]
    sql = """INSERT INTO IVcourseWork1.trainData_251119(departureStationName, departureStationCRS, 
                                  arrivalStationName, arrivalStationCRS, departureTime, 
                                  arrivalTime, durationHours, durationMinutes, changes,
                                  breakdownType, fareTicketType, fareProvider, ticketPrice)
                   VALUES """ + values + """;"""
    # Open database connection
    db = pymysql.connect("192.168.2.249", "root", "123456", "test")

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    # Prepare SQL query to INSERT a record into the database.
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Commit your changes in the database
        db.commit()
        print('success!!!')
    except:
        # Rollback in case there is any error
        db.rollback()

    # disconnect from server
    db.close()


# input all of the citys
cityList = [
    'Cardiff',
    'Nottingham',
    'Norwich',
    'London',
    'Newcastle',
    'Manchester',
    'Edinburgh',
    'Southampton',
    'Bristol',
    'Birmingham',
    'Leeds'
]
# get 110 combinations
cityCombination = []
for i in cityList:
    for j in cityList:
        if i != j:
            cityCombination.append(i + '/' + j)


def multicore():
    # avoid duplications
    unseen = set(cityCombination)
    seen = set()
    pool = mp.Pool(4)
    count, start_time = 0, time.time()
    crawl_jobs = [pool.apply_async(getJourneys, args=(city,)) for city in unseen]
    # the length of journey_list is 110
    journey_list = [job.get() for job in crawl_jobs]
    seen.update(unseen)
    unseen.clear()
    # sql(journey_list)
    init_url_dict = getInitUrl()
    date = init_url_dict['url2'].replace('/', '')
    filename = date + ' trainData.csv'
    result = []
    for journey in journey_list:
        for i in journey:
            count += 1
            result.append(i)
    writeCsv(result, filename)
    print('Total time : %.1f s' % (time.time() - start_time) + 'Total count : ' + str(count))


if __name__ == '__main__':
    multicore()
