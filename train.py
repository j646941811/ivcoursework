import pymysql
import requests
from bs4 import BeautifulSoup
import csv
import json
import re

allNeedHeader = ['departureStationName', 'departureStationCRS', 'arrivalStationName', 'arrivalStationCRS',
                 'departureTime', 'arrivalTime', 'durationHours', 'durationMinutes', 'changes',
                 'breakdownType', 'fareTicketType', 'fareProvider', 'ticketPrice']


def getInitUrl():
    # link fomat : 'http://ojp.nationalrail.co.uk/service/timesandfares/leeds/London/140120/0000/dep'
    url1 = 'http://ojp.nationalrail.co.uk/service/timesandfares/'
    url2 = '/140120/'
    url3 = '0000'
    url4 = '/dep'
    UserAgents = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36'
    ]
    headers = {
        'user-agent': UserAgents[0],
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.9,zh-TW;q=0.8,en-US;q=0.7,en;q=0.6',
        'upgrade-insecure-requests': '1',
        'host': 'ojp.nationalrail.co.uk'
    }
    return {
        'url1': url1,
        'url2': url2,
        'url3': url3,
        'url4': url4,
        'headers': headers
    }


TrainInfos = []


def getJourneys(journey, urlDict, originalTime=0000):
    url = urlDict['url1'] + journey[0] + '/' + journey[1] + urlDict['url2'] + urlDict['url3'] + urlDict[
        'url4']
    print('start to request ' + url)
    # only return 5 Journeys after the specific time(0000) each request (So the bug needs to be fixed here)
    response = requests.get(url, headers=urlDict['headers'])
    soup = BeautifulSoup(response.text, features='lxml')
    dataList = soup.find(attrs={'id': 'oft'}).find('tbody').find_all('tr', {'class': re.compile('.*mtx.*')})
    trainInfo = {}
    for key, rowData in enumerate(dataList):
        tdList = rowData.find_all('td')
        # don not delete this comment!!!
        # trainInfo['dep'] = tdList[0].get_text().strip()  # dep time
        # trainInfo['from'] = tdList[1].get_text().strip() + tdList[1].find('abbr').get_text().strip()  # from city
        # trainInfo['to'] = tdList[2].get_text().strip() + tdList[2].find('abbr').get_text().strip()  # to city
        # trainInfo['arr'] = tdList[3].get_text().strip()  # arr time
        # trainInfo['dur'] = tdList[4].get_text().strip()  # dur time
        # trainInfo['info'] = tdList[6].find('a').get_text().strip()  # details info
        trainInfo['allinfo'] = tdList[8].find('script').get_text().strip()  # all info
        allInfoDict = json.loads(trainInfo['allinfo'])
        allInfo = {}
        allNeedInfo = {}
        # 将journey信息和价格信息的字典拼接一起
        allInfo.update(allInfoDict['jsonJourneyBreakdown'])
        if len(allInfoDict['singleJsonFareBreakdowns']) != 0:
            allInfo.update(allInfoDict['singleJsonFareBreakdowns'][0])
        # 只保留想要的字段
        for item in allNeedHeader:
            if item in allInfo.keys():
                allNeedInfo[item] = allInfo[item]
            else:
                allNeedInfo[item] = 'NaN'
        # 判断有没有必要继续添加，防止加入第二天的数据
        if urlDict['url3'] == '0000' and key == 0:
            originalTime = int(allNeedInfo['departureTime'].replace(':', ''))
        departureTime = int(allNeedInfo['departureTime'].replace(':', ''))
        if departureTime >= originalTime:
            originalTime = departureTime
            TrainInfos.append(allNeedInfo)
        else:
            print(journey[0] + ' to ' + journey[1] + 'is done')
            return TrainInfos
    # This is the program to fix the bug
    # get the time of the last journey and add 6 minutes as the condition for the next request,
    # if not, it would result some duplications in the journeys
    # 6 minutes is really tricky!!! But this is the truth in the website
    print('get ' + urlDict['url3'] + ' data successful')
    departureTime = int(TrainInfos[-1]['departureTime'].replace(':', '')) + 6
    if departureTime < 2359:
        departureTime = str(departureTime).zfill(4)
        if departureTime[2] == '6':
            # 取出departureTime的hour
            hour = int(departureTime[0] + departureTime[1])
            hour = hour + 1
            departureTime = (str(hour) + '0' + departureTime[3]).zfill(4)
        urlDict['url3'] = departureTime
        print('next departureTime ' + departureTime)
        # 递归函数的坑，谨记谨记！
        return getJourneys(journey, urlDict, originalTime)
    else:
        return TrainInfos


def writeCsv(result, filename):
    print('start write ' + filename)
    with open(filename, "w", encoding='utf8', newline='') as outFileCsv:
        # 设置csv表头
        fileheader = allNeedHeader
        outDictWriter = csv.DictWriter(outFileCsv, fileheader)
        outDictWriter.writeheader()
        # 设置csv数据,这里的数据格式是字典型
        outDictWriter.writerows(result)
        outFileCsv.close()
        print('write success!!!')


def sql(dictList):
    sqlStr = ''
    values = ''
    for dict in dictList:
        for item in allNeedHeader:
            sqlStr = sqlStr + "'" + str(dict[item]) + "',"
        values = values + '(' + sqlStr[:-1] + '),'
    values = values[:-1]
    sql = """INSERT INTO IVcourseWork1.trainData_251119(departureStationName, departureStationCRS, 
                                  arrivalStationName, arrivalStationCRS, departureTime, 
                                  arrivalTime, durationHours, durationMinutes, changes,
                                  breakdownType, fareTicketType, fareProvider, ticketPrice)
                   VALUES """ + values + """;"""
    # Open database connection
    db = pymysql.connect("192.168.2.249", "root", "123456", "test")

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    # Prepare SQL query to INSERT a record into the database.
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Commit your changes in the database
        db.commit()
        print('success!!!')
    except:
        # Rollback in case there is any error
        db.rollback()

    # disconnect from server
    db.close()


# input all of the citys
cityList = [
    'Cardiff',
    'Nottingham',
    'Norwich',
    'London',
    'Newcastle',
    'Manchester',
    'Edinburgh',
    'Southampton',
    'Bristol',
    'Birmingham',
    'Leeds'
]
# get 110 combinations
cityCombination = []
for i in cityList:
    for j in cityList:
        if i != j:
            cityCombination.append([i, j])

journeyList = []
initUrlDict = getInitUrl()
for journey in cityCombination:
    journeyList = getJourneys(journey, initUrlDict)
    # This is necessary to init the value of url3 each time because getJourneys() would change the value of initUrlDict
    initUrlDict['url3'] = '0000'
# sql(result)
date = initUrlDict['url2'].replace('/', '')
filename = date + ' trainData.csv'
writeCsv(journeyList, filename)
