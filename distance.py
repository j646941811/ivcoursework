import googlemaps
import pymysql
import csv
from configparser import ConfigParser
from math import *
from geopy.distance import geodesic

cityList = [
    'Cardiff',
    'Nottingham',
    'Norwich',
    'London',
    'Newcastle',
    'Manchester',
    'Edinburgh',
    'Southampton',
    'Bristol',
    'Birmingham',
    'Leeds'
]

config = ConfigParser()
config.read('sys.ini', encoding='utf-8')
if not config.has_option('geogle', 'key'):
    print('please check your configuration')
    exit()


def sqlRead():
    # Open database connection
    db = pymysql.connect(config['db']['host'], config['db']['user'], config['db']['pass'], "IVcourseWork1")

    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    # 按字典返回
    # cursor = db.cursor(pymysql.cursors.DictCursor)

    # Prepare SQL query to select a record from the table.
    sql = """select cityName, lat, lng from IVcourseWork1.lat_lng;"""
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Fetch all the rows in a list of lists.
        results = cursor.fetchall()
        return results
    except:
        import traceback
        traceback.print_exc()

        print("Error: unable to fetch data")

    # disconnect from server
    db.close()


def sqlInsert(dictList):
    sqlStr = ''
    values = ''
    for dict in dictList:
        # {'Cardiff': {'lat': '51.48158100000001, 'lng': -3.17909, 'status': 0}}
        if dictList[dict]['status'] == 1:
            sqlStr = "'" + dict + "','" + str(dictList[dict]['lat']) + "','" + str(dictList[dict]['lng']) + "'"
            values = values + '(' + sqlStr + '),'
    if values == '':
        print('Nothing insert')
        return
    values = values[:-1]
    sql = """insert into IVcourseWork1.lat_lng(cityName, lat, lng) values """ + values + """;"""
    # Open database connection
    db = pymysql.connect(config['db']['host'], config['db']['user'], config['db']['pass'], "IVcourseWork1")

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    # Prepare SQL query to INSERT a record into the database.
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Commit your changes in the database
        db.commit()
        print('insert success!!!')
    except:
        # Rollback in case there is any error
        db.rollback()

    # disconnect from server
    db.close()


def writeCsv(result, fileheader, filename):
    print('start to write ' + filename + '!')
    with open(filename, "w", encoding='utf8', newline='') as outFileCsv:
        outDictWriter = csv.DictWriter(outFileCsv, fileheader)
        outDictWriter.writeheader()
        # setting csv data,data format here is dict
        outDictWriter.writerows(result)
        outFileCsv.close()
    print('write successful!')


# fetchall
# ('Cardiff', '51.48158100000001', '-3.17909'), ('Nottingham', '52.95478319999999', '-1.1581086')
sqlResult = sqlRead()
sqlResultDict = {}
if len(sqlResult) != 0:
    for result in sqlResult:
        sqlResultDict[result[0]] = (result[1], result[2])

gmaps = googlemaps.Client(config['geogle']['key'])
# Geocoding an address
geocodeResult = {}
for city in cityList:
    geocodeResult[city] = {}
    if sqlResultDict.get(city):
        geocodeResult[city]['lat'] = sqlResultDict[city][0]
        geocodeResult[city]['lng'] = sqlResultDict[city][1]
        geocodeResult[city]['status'] = 0  # status = 0 means don't need insert
    else:
        jsonResult = gmaps.geocode(city + ',uk')
        if len(jsonResult) != 0:
            geocodeResult[city]['lat'] = jsonResult[0]['geometry']['location']['lat']  # Latitude
            geocodeResult[city]['lng'] = jsonResult[0]['geometry']['location']['lng']  # Longitude
            geocodeResult[city]['status'] = 1
        else:
            print('get lat-lng by mistake，recheck！')
            exit()

# insert lat-lng
sqlInsert(geocodeResult)

# generate csv
# metadata format
# result = {
# 'Cardiff': {'lat': 51.48158100000001, 'lng': -3.17909, 'status': 0},
# 'Leeds': {'lat': 51.48158100000001, 'lng': -3.17909, 'status': 0}
# }
# csv data format
# result = [
#     {'cityName': 'Cardiff', 'lat': 51.48158100000001, 'lng': -3.17909},
#     {'cityName': 'Leeds', 'lat': 51.48158100000001, 'lng': -3.17909}
# ]
# writeCsv(result)
# exit()
# deal with data format
writeResult = []
for city in geocodeResult:
    geocodeResult[city].update({'cityName': city})
    geocodeResult[city].pop('status')
    writeResult.append(geocodeResult[city])
fileheader = ['cityName', 'lat', 'lng']
filename = 'lat_lng.csv'
writeCsv(writeResult, fileheader, filename)

# Loading the lat-long data for Kolkata & Delhi
# kolkata = (22.5726, 88.3639)
# delhi = (28.7041, 77.1025)
#
# Print the distance calculated in km
# print(geodesic(kolkata, delhi).km)
# exit()

# point list
# ['Cardiff':(lat,lng),'Leeds':(lat,lng)]
pointArr = {}
for city in geocodeResult:
    pointArr[city] = (geocodeResult[city]['lat'], geocodeResult[city]['lng'])

# calculate distance according to lat-lng
distanceArr = []
for i in pointArr:
    for j in pointArr:
        if i != j:
            distance = geodesic(pointArr[i], pointArr[j]).miles
            distanceArr.append({'city1': i, 'city2': j, 'distance': distance})

fileheader = ['city1', 'city2', 'distance']
filename = 'distance.csv'
writeCsv(distanceArr, fileheader, filename)

# input Lat_A 纬度A
# input Lng_A 经度A
# input Lat_B 纬度B
# input Lng_B 经度B
# output distance 距离(km)
"""
基于googleMap中的算法得到两经纬度之间的距离
"""
# def calcDistance(Lat_A, Lng_A, Lat_B, Lng_B):
#     EARTH_RADIUS = 6378.137  # 赤道半径(单位km)
#     radLat1 = radians(Lat_A)
#     radLat2 = radians(Lat_B)
#     a = radLat1 - radLat2
#     b = radians(Lng_A) - radians(Lng_B)
#
#     s = 2 * asin(sqrt(pow(sin(a / 2), 2) + cos(radLat1) * cos(radLat2) * pow(sin(b / 2), 2)));
#     s = s * EARTH_RADIUS
#     # s = Math.round(s * 10000d) / 10000d;
#     return s
#
#
# Lat_A = 32.060255
# Lng_A = 118.796877  # 南京
# Lat_B = 39.904211
# Lng_B = 116.407395  # 北京
# distance = calcDistance(Lat_A, Lng_A, Lat_B, Lng_B)
# print("----", distance)
