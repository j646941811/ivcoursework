import csv
import pymysql
from configparser import ConfigParser
from geopy.distance import geodesic

config = ConfigParser()
config.read('sys.ini', encoding='utf-8')


def sqlRead():
    # Open database connection
    db = pymysql.connect(config['db']['host'], config['db']['user'], config['db']['pass'], "IVcourseWork1")

    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    # 按字典返回
    # cursor = db.cursor(pymysql.cursors.DictCursor)

    # Prepare SQL query to select a record from the table.
    sql = """select cityName, lat, lng from IVcourseWork1.lat_lng;"""
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Fetch all the rows in a list of lists.
        results = cursor.fetchall()
        return results
    except:
        import traceback
        traceback.print_exc()

        print("Error: unable to fetch data")

    # disconnect from server
    db.close()


# fetchall
# ('Cardiff', '51.48158100000001', '-3.17909'), ('Nottingham', '52.95478319999999', '-1.1581086')
sqlResult = sqlRead()
pointArr = {}
if len(sqlResult) != 0:
    for result in sqlResult:
        # {'Cardiff':(lat,lng),'Leeds':(lat,lng)}
        pointArr[result[0]] = (result[1], result[2])

# calculate distance according to lat-lng
distanceArr = []
for i in pointArr:
    for j in pointArr:
        if i != j:
            distance = geodesic(pointArr[i], pointArr[j]).miles
            distanceArr.append({'city1': i, 'city2': j, 'distance': distance})


